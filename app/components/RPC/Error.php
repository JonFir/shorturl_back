<?php
declare(strict_types=1);

namespace Components\RPC;


/**
 * Class Error
 * @package library\RPC
 */
class Error
{
    /**
     * @var int
     */
    protected $code;
    /**
     * @var string
     */
    protected $message;

    /**
     * Error constructor.
     * @param int $code
     * @param string $message
     */
    public function __construct(int $code, string $message)
    {
        $this->code = $code;
        $this->message = $message;
    }

    /**
     * @return array
     */
    public function getContent(): array
    {
        return [
            'code' => $this->code,
            'message' => $this->message
        ];
    }

}
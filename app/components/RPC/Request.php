<?php
declare(strict_types=1);

namespace Components\RPC;

use Phalcon\Http\Request as MVCRequest;

/**
 * Class Request
 * @package Components\RPC
 *
 */
class Request extends MVCRequest
{
    /**
     * @var string
     */
    protected $controller;
    /**
     * @var string
     */
    protected $action;
    /**
     * @var array
     */
    protected $params;
    /**
     * @var int|null
     */
    protected $id;

    /**
     * RPCRequest constructor.
     * @param MVCRequest $request
     * @throws Exception
     */
    public function __construct(MVCRequest $request)
    {
        try {
            $json = $request->getJsonRawBody();
            if (!isset($json->jsonrpc)) {
                throw new Exception();
            }
            $method = explode('.', $json->method);
            $this->controller = $method[0];
            $this->action = $method[1];
            $this->params = isset($json->params) ? (array)$json->params : [];
            $this->id = $json->id;
        } catch (\Exception $exception) {
            throw new Exception("The request is not Json-RPC", 2);
        }
    }

    /**
     * @return string
     */
    public function getController(): string
    {
        return $this->controller;
    }

    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

}
<?php
declare(strict_types=1);

namespace Components\RPC;

use Phalcon\Http\Response as MVCResponse;
use Phalcon\Http\ResponseInterface as MVCResponseInterface;


/**
 * Class Response
 * @package library\RPC
 */
class Response extends MVCResponse
{
    /**
     * @var mixed|null
     */
    protected $result = null;
    /**
     * @var Error|null
     */
    protected $error = null;
    /**
     * @var int|null
     */
    protected $id = null;

    /**
     * Response constructor.
     * @param string|null $content
     * @param int|null $code
     * @param string|null $status
     * @throws Exception
     */
    public function __construct($content = null, $code = null, $status = null)
    {
        parent::__construct(null, $code, $status);
        $this->setContentType('application/json', 'UTF-8');
        if (isset($content)) {
            $this->setContent($content);
        }
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param mixed $result
     * @return self
     * @throws Exception
     */
    public function setResult($result): self
    {
        $this->result = $result;
        $this->fillContent();
        return $this;
    }

    /**
     * @return Error|null
     */
    public function getError(): ?Error
    {
        return $this->error;
    }

    /**
     * @param Error|null $error
     * @return Response
     * @throws Exception
     */
    public function setError(?Error $error): self
    {
        $this->error = $error;
        $this->fillContent();
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Response
     * @throws Exception
     */
    public function setId(?int $id): self
    {
        $this->id = $id;
        $this->fillContent();
        return $this;
    }

    /**
     * @param string $content
     * @return MVCResponseInterface
     * @throws Exception
     */
    public function setContent(string $content): MVCResponseInterface
    {
        $this->setResult($content);
        return $this;
    }

    /**
     * Приходится использовать такой костыль, так как ответ клиенту формируется напрямую из свойства $content.
     * Если бы использовался геттер getContent, я бы его переопределил. Возможно есть другие пути, но есть лимит времени
     * который можно потратить на ТЗ.
     * @throws Exception
     */
    private function fillContent()
    {
        $result = $this->getResult();
        $error = $this->getError();
        $error = isset($error) ? $error->getContent() : null;

        if (isset($result) && isset($error)) {
            throw new Exception("Request cant have result and error at same time",1);
        }

        $json = [
            'jsonrpc' => "2.0", // номер версии конечно вынести надо в настройки, но для ТЗ сойдет.
            'result' => $result,
            'error' => $error,
            'id' => $this->getId()
        ];
        parent::setContent(json_encode($json));
    }


}
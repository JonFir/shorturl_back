<?php
declare(strict_types=1);

namespace Components\URL;

/**
 * Методы честно взяты из сети.
 * Class URL
 * @package Components
 */
final class URL
{

    /**
     * Кодирует целое число в короткую ссылку.
     * @param int $id
     * @return string
     * @throws Exception
     */
    static function urlFormId(int $id): string
    {
        if ($id < 0 || !is_int($id)) {
            throw new Exception('id should be positive int number', 1);
        }
        $digits = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $link = '';
        do {
            $dig = $id % 52;
            $link = $digits[$dig] . $link;
            $id = floor($id / 52);
        } while ($id != 0);
        return $link;
    }

    /**
     * Декодирует короткую ссылку в целое число.
     * @param string $url
     * @return int
     * @throws Exception
     */
    static function idFromUrl(string $url): int
    {
        $digits = [
            'a' => 0,
            'b' => 1,
            'c' => 2,
            'd' => 3,
            'e' => 4,
            'f' => 5,
            'g' => 6,
            'h' => 7,
            'i' => 8,
            'j' => 9,
            'k' => 10,
            'l' => 11,
            'm' => 12,
            'n' => 13,
            'o' => 14,
            'p' => 15,
            'q' => 16,
            'r' => 17,
            's' => 18,
            't' => 19,
            'u' => 20,
            'v' => 21,
            'w' => 22,
            'x' => 23,
            'y' => 24,
            'z' => 25,
            'A' => 26,
            'B' => 27,
            'C' => 28,
            'D' => 29,
            'E' => 30,
            'F' => 31,
            'G' => 32,
            'H' => 33,
            'I' => 34,
            'J' => 35,
            'K' => 36,
            'L' => 37,
            'M' => 38,
            'N' => 39,
            'O' => 40,
            'P' => 41,
            'Q' => 42,
            'R' => 43,
            'S' => 44,
            'T' => 45,
            'U' => 46,
            'V' => 47,
            'W' => 48,
            'X' => 49,
            'Y' => 50,
            'Z' => 51
        ];
        $id = 0;
        try {
            for ($i = 0; $i < strlen($url); $i++) {
                $index = $digits[$url[(strlen($url) - $i - 1)]];
                if (!isset($index)) {
                    throw new Exception();
                }
                $id += $index * pow(52, $i);
            }
        } catch (\Exception $e) {
            throw new Exception('url should contains only a-Z characters', 2);
        }

        return $id;
    }

}


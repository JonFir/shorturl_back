<?php
declare(strict_types=1);

use Components\RPC\Error;
use Components\RPC\Response;
use Components\RPC\Exception as RPCException;
use Phalcon\Mvc\Controller;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Url as UrlValidator;
use Components\URL\URL as UrlShorter;
use Components\URL\Exception as URLException;

/**
 * Class UrlController
 * Не покрыт тестами, здесь надо было бы либо мокать бд, либо втаскивать ее в тесты, но как оказалось с тестами
 * в 4ой версии фалкона беда, дока устарела, хелпер либы я найти не смог,
 * она проде как не поддерживается для этой версии.
 * Миграции нормально не подклюаются. Опять же можно было написать скрипты создания и дропа тестовой схемы руками,
 * но не хочется тратить столько времени на тестовое задание.
 */
class UrlController extends Controller
{

    /**
     * Поиск полной url по короткой версии.
     * @return Response
     * @throws RPCException
     */
    public function GetAction()
    {
        /** @var Response $response */
        $response = $this->response;
        $url = $this->dispatcher->getParam('url');
        if (!isset($url)) {
            return $this->setError(1, '`url` parameter required', $response);
        }
        if (!is_string($url)) {
            return $this->setError(5, '`url` parameter must be string', $response);
        }
        try {
            $id = UrlShorter::idFromUrl($url);
            if ($url = Url::findFirst($id)) {
                $response->setResult(["url" => $url->full_url]);
            } else {
                $response = $this->setError(2, 'url not found', $response);
            }
        } catch (RPCException $exception) {
            if ($exception->getCode() === 1) {
                throw $exception;
            } else if ($exception->getCode() === 2) {
                $response = $this->setError(3, $exception->getMessage(), $response);
            }
        } catch (URLException $exception) {
            $response = $this->setError(4, $exception->getMessage(), $response);
        } catch (PDOException $exception) {
            throw $exception;
        }

        return $response;
    }

    /**
     * Создание новой короткой ссылки.
     * @return Response
     * @throws RPCException
     */
    public function CreateAction()
    {
        /** @var Response $response */
        $response = $this->response;
        $parameters = $this->dispatcher->getParams();
        if (!$this->validateCreateParameters($parameters)) {
            return $this->setError(5, '`url` parameter must be URL', $response);
        }

        try {
            $urlFromRequest = $parameters['url'];
            // Не стал делать валидатором, так как надо не просто проверить на уникальность,
            // но и вернуть ссылку, если такая уже есть в базе.
            $url = $url = Url::findFirst(['full_url = :url:', 'bind' => ['url' => $urlFromRequest]]);
            if (!isset($url)) {
                $url = new Url();
                $url->full_url = $urlFromRequest;
                if ($url->create() === false) {
                    throw new PDOException();
                }
            }

            $url = UrlShorter::urlFormId($url->getId());
            $response->setResult(["url" => $url]);
        } catch (RPCException $exception) {
            if ($exception->getCode() === 1) {
                throw $exception;
            } else if ($exception->getCode() === 2) {
                $response = $this->setError(3, $exception->getMessage(), $response);
            }
        } catch (URLException $exception) {
            $response = $this->setError(4, $exception->getMessage(), $response);
        } catch (PDOException $exception) {
            throw $exception;
        }

        return $response;
    }

    /**
     * @param array $parameters
     * @return bool
     */
    private function validateCreateParameters(array $parameters): bool
    {
        $parameterName = "url";

        $validation = new Validation();
        $validation->add($parameterName, new UrlValidator());
        $messages = $validation->validate($parameters);

        return count($messages) == 0;
    }

    /**
     * @param int $code
     * @param string $message
     * @param Response $response
     * @return Response
     * @throws RPCException
     */
    private function setError(int $code, string $message, Response $response): Response
    {
        $error = new Error($code, $message);
        $response->setError($error);
        return $response;
    }

}


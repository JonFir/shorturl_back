<?php

use Phalcon\Di;
use Phalcon\Di\FactoryDefault;
use Phalcon\Loader;

ini_set("display_errors", 1);
error_reporting(E_ALL);

define('BASE_PATH', dirname(__DIR__));
define("ROOT_PATH", __DIR__);
define('APP_PATH', BASE_PATH . '/app');

set_include_path(
    ROOT_PATH . PATH_SEPARATOR . get_include_path()
);

$config = new \Phalcon\Config([
    'database' => [
        'adapter' => 'Postgresql',
        'host' => 'localhost',
        'username' => 'shorturl',
        'password' => '123456',
        'dbname' => 'shorturl',
        'schema' => 'tests'
    ],
    'application' => [
        'appDir' => APP_PATH . '/',
        'controllersDir' => APP_PATH . '/controllers/',
        'modelsDir' => APP_PATH . '/models/',
        'migrationsDir' => APP_PATH . '/migrations/',
        'viewsDir' => APP_PATH . '/views/',
        'componentsDir' => APP_PATH . '/components/',
        'pluginsDir' => APP_PATH . '/plugins/',
        'libraryDir' => APP_PATH . '/library/',
        'cacheDir' => BASE_PATH . '/cache/',
        'baseUri' => '/',
    ]
]);

// Используем автозагрузчик приложений для автозагрузки классов.
$loader = new Loader();

$loader->registerDirs(
    [
        ROOT_PATH,
        $config->application->controllersDir,
        $config->application->modelsDir
    ]
)->registerNamespaces(
    [
        'Components' => $config->application->componentsDir
    ]
)->register();

$di = new FactoryDefault();

Di::reset();

Di::setDefault($di);
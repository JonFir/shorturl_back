<?php

namespace Components\RPC;


class RequestTest extends \UnitTestCase
{

    public function testFullRequest()
    {
        // given

        $body = [
            'jsonrpc' => '2.0',
            'method' => 'testController.testAction',
            'params' => 'test',
            'id' => 1
        ];
        $mvcRequest = new MVCRequestMock($body);

        // when

        $request = new Request($mvcRequest);

        // then

        $this->assertSame($request->getId(), 1);
        $this->assertSame($request->getParams()[0], 'test');
        $this->assertSame($request->getController(), 'testController');
        $this->assertSame($request->getAction(), 'testAction');
    }

    public function testMinimalRequest()
    {
        // given

        $body = [
            'jsonrpc' => '2.0',
            'method' => 'testController.testAction',
            'id' => null
        ];
        $mvcRequest = new MVCRequestMock($body);

        // when

        $request = new Request($mvcRequest);

        // then

        $this->assertSame($request->getController(), 'testController');
        $this->assertSame($request->getAction(), 'testAction');
        $this->assertIsArray($request->getParams());
        $this->assertCount(0, $request->getParams());
        $this->assertNull($request->getId());
    }

    public function testShouldHaveJsonrpc()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionCode(2);

        // given

        $body = [
            'method' => 'testController.testAction',
            'id' => null
        ];
        $mvcRequest = new MVCRequestMock($body);

        // when

        $request = new Request($mvcRequest);

        // then
    }

    public function testShouldHavePath()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionCode(2);

        // given

        $body = [
            'jsonrpc' => '2.0',
            'method' => 'test',
            'id' => null
        ];
        $mvcRequest = new MVCRequestMock($body);

        // when

        $request = new Request($mvcRequest);

        // then
    }

}


class MVCRequestMock extends \Phalcon\Http\Request
{

    /**
     * @var array
     */
    private $body;

    /**
     * MVCRequestMock constructor.
     * @param array $body
     */
    public function __construct(array $body)
    {
        $this->body = $body;
    }

    public function getJsonRawBody(bool $associative = false)
    {
        return json_decode(json_encode($this->body));
    }
}
<?php

namespace Components\RPC;


class ResponseTest extends \UnitTestCase
{

    public function testFull__construct()
    {
        // given

        $response = new Response('test', 1, 'test_status');

        // when

        $json = json_decode($response->getContent());

        // then

        $this->assertSame($json->jsonrpc, '2.0');
        $this->assertSame($json->result, 'test');
        $this->assertNull($json->error);
        $this->assertNull($json->id);
        $this->assertSame($response->getHeaders()->get('Content-Type'), 'application/json; charset=UTF-8');
        $this->assertSame($response->getHeaders()->get('Status'), '1 test_status');
        $this->assertSame($response->getResult(), 'test');
    }

    public function testContent__construct()
    {
        // given

        $response = new Response('test');

        // when

        $json = json_decode($response->getContent());

        // then

        $this->assertSame($json->jsonrpc, '2.0');
        $this->assertSame($json->result, 'test');
        $this->assertNull($json->error);
        $this->assertNull($json->id);
        $this->assertSame($response->getHeaders()->get('Content-Type'), 'application/json; charset=UTF-8');
        $this->assertFalse($response->hasHeader('Status'));
        $this->assertSame($response->getResult(), 'test');
    }

    public function testEmpty__construct()
    {
        // given

        $response = new Response();

        // when

        // then

        $this->assertNull($response->getContent());
        $this->assertSame($response->getHeaders()->get('Content-Type'), 'application/json; charset=UTF-8');
        $this->assertFalse($response->hasHeader('Status'));
    }

    public function testShouldRiseExceptionWrongContent()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionCode(1);

        // given

        $response = new Response();
        $error = new Error(23,'test');

        // when

        $response->setResult('test');
        $response->setError($error);

        // then
    }

    public function testResult()
    {
        // given

        $response = new Response();

        // when

        $response->setResult('test');

        // then

        $this->assertSame($response->getResult(), 'test');
    }

    public function testId()
    {
        // given

        $response = new Response();

        // when

        $response->setId(1);

        // then

        $this->assertSame($response->getId(), 1);
    }

    public function testContent()
    {
        // given

        $response = new Response();

        // when

        $response->setContent('test');

        // then

        $this->assertSame($response->getResult(), 'test');
    }

    public function testError()
    {
        // given

        $response = new Response();
        $error = new Error(23,'test');

        // when

        $response->setError($error);
        $responseError = $response->getError();

        // then

        $this->assertSame($responseError->getContent(), $error->getContent());
    }

}

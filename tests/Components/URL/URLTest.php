<?php

namespace Components\URL;


class URLTest extends \UnitTestCase
{
    /**
     * @dataProvider randomIdProvider
     */
    public function testShouldDecodeAndEncode($id)
    {
        $url = URL::urlFormId($id);
        $decodedId = URL::idFromUrl($url);
        $this->assertSame($id, $decodedId);
    }

    public function randomIdProvider()
    {
        $array = [];
        for ($i = 0; $i <= 20; $i++) {
            $array[] = [random_int(100, 999999)];
        }
        return $array;
    }

    public function testShouldMakeUrl()
    {
        $url = URL::urlFormId(317950);
        $this->assertSame($url, 'cnEw');
    }

    public function testShouldDecodeId()
    {
        $id = URL::idFromUrl('Gbd');
        $this->assertSame($id, 86583);
    }

    public function testShouldRiseExceptionOnNegativeId() {
        $this->expectException(Exception::class);
        $this->expectExceptionCode(1);
        URL::urlFormId(-317950);
    }

    public function testShouldRiseExceptionOnWrongUrl() {
        $this->expectException(Exception::class);
        $this->expectExceptionCode(2);
        URL::idFromUrl("аааа");
    }

    public function testShouldRiseExceptionOnIntInUrl() {
        $this->expectException(Exception::class);
        $this->expectExceptionCode(2);
        URL::idFromUrl("-12");
    }
}

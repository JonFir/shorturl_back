<?php

use Phalcon\Di;
use PHPUnit\Framework\TestCase;

abstract class UnitTestCase extends TestCase
{
    /**
     * @var bool
     */
    private $_loaded = false;
    /**
     * @var Phalcon\Di
     */
    private $di;

    public function setUp(): void
    {
        parent::setUp();
        $this->setDi(Di::getDefault());
    }

    /**
     * @return Di
     */
    public function getDi(): Di
    {
        return $this->di;
    }

    /**
     * @param Di $di
     */
    public function setDi(Di $di): void
    {
        $this->di = $di;
    }
}
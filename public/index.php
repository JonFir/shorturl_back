<?php
declare(strict_types=1);

use Phalcon\Di\FactoryDefault;
use Phalcon\Http\ResponseInterface;
use Components\RPC\Request as PRCRequest;
use Components\RPC\Response;


error_reporting(E_ALL);

define('BASE_PATH', dirname(__DIR__));
define('APP_PATH', BASE_PATH . '/app');

try {
    /**
     * The FactoryDefault Dependency Injector automatically registers
     * the services that provide a full stack framework.
     */
    $di = new FactoryDefault();

    /**
     * Read services
     */
    include APP_PATH . '/config/services.php';

    /**
     * Get config service for use in inline setup below
     */
    $config = $di->getConfig();

    /**
     * Include Autoloader
     */
    include APP_PATH . '/config/loader.php';

    /**
     * @var PRCRequest $request
     */
    $request = new PRCRequest($di->getShared("request"));
    $di->setShared('response', function () use ($request) {
        $response = new Response();
        $response->setDI($this);
        $response->setId($request->getId());
        return $response;
    });

    $dispatcher = $di['dispatcher'];

    $dispatcher->setControllerName($request->getController());
    $dispatcher->setActionName($request->getAction());
    $dispatcher->setParams($request->getParams());

    $dispatcher->dispatch();

    /** @var Response $response */
    $response = $dispatcher->getReturnedValue();

    if ($response instanceof ResponseInterface) {
        $response->send();
    }

} catch (\Exception $e) {
    echo $e->getMessage() . '<br>';
    echo '<pre>' . $e->getTraceAsString() . '</pre>';
}